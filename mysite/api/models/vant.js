const mongoose = require('mongoose');

const vantSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    value: { type: Number, require: true},
    unitate: { type: String, require: true}
});

module.exports = mongoose.model('Vant', vantSchema);