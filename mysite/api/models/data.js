const mongoose = require('mongoose');

const temperaturaSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    value: { type: Number, require: true},
    type: { type: String, require: true}
});

module.exports = mongoose.model('Tempertaura', temperaturaSchema);