const mongoose = require('mongoose');

const umiditateSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    value: { type: Number, require: true},
    procent: { type: String, require: true}
});

module.exports = mongoose.model('Umiditate', umiditateSchema);