const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
        res.status(200).json({
            Temperatura: '= 23 C',
            Umiditatea: '=65%',
            VitezaVantaului: '=23 KmH'
        });
});

router.get('/luni', (req, res, next) => {
    res.status(200).json({
        Temperatura: '= 27 C',
        Umiditatea: '=68%',
        VitezaVantaului: '=3 KmH'
    });
});

router.put('/luni', (req, res, next) => {
    const temperatura = {
        value: req.body.value,
        type: req.body.type
    };
    const umiditatea = {
        value: req.body.value,
        procent: req.body.procent
    };
    const vant = {
        value: req.body.value,
        unitate: req.body.unitate
    };
    res.status(200).json({
        message: "PUT ACCTION",
        Temperatura: temperatura,
        Umiditatea: umiditatea,
        Vant: vant
    });
});

router.get('/marti', (req, res, next) => {
    res.status(200).json({
        Temperatura: '= 16 C',
        Umiditatea: '=34%',
        VitezaVantaului: '=9 KmH'
    });
});

router.put('/marti', (req, res, next) => {
    const temperatura = {
        value: req.body.value,
        type: req.body.type
    };
    const umiditatea = {
        value: req.body.value,
        procent: req.body.procent
    };
    const vant = {
        value: req.body.value,
        unitate: req.body.unitate
    };
    res.status(200).json({
        message: "PUT ACCTION",
        Temperatura: temperatura,
        Umiditatea: umiditatea,
        Vant: vant
    });
});

router.get('/mircuri', (req, res, next) => {
    res.status(200).json({
        Temperatura: '= 28 C',
        Umiditatea: '=32%',
        VitezaVantaului: '=53 KmH'
    });
});

router.put('/mircuri', (req, res, next) => {
    const temperatura = {
        value: req.body.value,
        type: req.body.type
    };
    const umiditatea = {
        value: req.body.value,
        procent: req.body.procent
    };
    const vant = {
        value: req.body.value,
        unitate: req.body.unitate
    };
    res.status(200).json({
        message: "PUT ACCTION",
        Temperatura: temperatura,
        Umiditatea: umiditatea,
        Vant: vant
    });
});

router.get('/joi', (req, res, next) => {
    res.status(200).json({
        Temperatura: '= 215 C',
        Umiditatea: '=88%',
        VitezaVantaului: '=2 KmH'
    });
});

router.put('/joi', (req, res, next) => {
    const temperatura = {
        value: req.body.value,
        type: req.body.type
    };
    const umiditatea = {
        value: req.body.value,
        procent: req.body.procent
    };
    const vant = {
        value: req.body.value,
        unitate: req.body.unitate
    };
    res.status(200).json({
        message: "PUT ACCTION",
        Temperatura: temperatura,
        Umiditatea: umiditatea,
        Vant: vant
    });
});

router.get('/vineri', (req, res, next) => {
    res.status(200).json({
        Temperatura: '= 14 C',
        Umiditatea: '=44%',
        VitezaVantaului: '=15 KmH'
    });
});

router.put('/vineri', (req, res, next) => {
    const temperatura = {
        value: req.body.value,
        type: req.body.type
    };
    const umiditatea = {
        value: req.body.value,
        procent: req.body.procent
    };
    const vant = {
        value: req.body.value,
        unitate: req.body.unitate
    };
    res.status(200).json({
        message: "PUT ACCTION",
        Temperatura: temperatura,
        Umiditatea: umiditatea,
        Vant: vant
    });
});
module.exports = router;