const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Umiditate = require('../models/umiditate.js');


router.get('/', (req, res, next) => {
    Umiditate.find()
        .exec()
        .then(docs => {
            console.log(docs);
            const response = {
                count: docs.length,
                umiditate: docs.map(doc => {
                    return {
                        value: doc.value,
                        procent: doc.procent,
                        _id: doc._id,
                        request: {
                            type: 'GET',
                            url: 'http://192.168.111.11:7001/umiditate/' + doc._id
                        }

                    }
                })
            }
            res.status(200).json(response);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
})

router.get('/:umiditateId', (req, res, next) => {
    const id = req.params.umiditateId;
    Umiditate.findById(id)
        .exec()
        .then(doc => {
            console.log('From DB', doc);
            if (doc) {
                res.status(200).json({
                    value: doc,
                    request: {
                        type: 'GET',
                        description: 'Afiseaza umiditatea',
                        url: 'http://192.168.111.11:7001/umiditate/'
                    }
                });
            } else {
                res.status(404).json({message: 'Datele nu exista'});
            }
            
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
});

router.post('/', (req, res, next) => {
   const umiditate = new Umiditate({
       _id: new mongoose.type.ObjectId(),
       value: req.body.value,
       procent: req.body.procent
   });
   umiditate
   .save()
   .then(result => {
       console.log(result);
       res.status(200).json({
        message: 'POST temp',
        createdUmiditate: {
            value: result.value,
            procent: result.procent,
            _id: result._id,
            request: {
                type: 'GET the new item',
                url: 'http://192.168.111.11:7001/umiditate/' + result._id
            } 
        }
    });
   })
   .catch(err => console.log(err => {
       console.log(err);
       res.status(500).json({
           error: err
       })
   }));
});

router.put('/', (req, res, next) => {
    const umiditate = new Umiditate({
        _id: new mongoose.type.ObjectId(),
        value: req.body.value,
        procent: req.body.procent
    });
    umiditate
    .save()
    .then(result => {
        console.log(result);
        res.status(200).json({
         message: 'POST temp',
         createdUmiditate: {
            value: result.value,
            procent: result.procent,
            _id: result._id,
            request: {
                type: 'GET the new item',
                url: 'http://192.168.111.11:7001/umiditate/' + result._id
            } 
        }
     });
    })
    .catch(err => console.log(err => {
        console.log(err);
        res.status(500).json({
            error: err
        })
    }));
 });

router.put('/:umiditateId', (req, res, next) => {
    const id = req.params.umiditateId;
    const updateOps = {};
    for (const ops of req.body) {
      updateOps[ops.propName] = ops.value;
    }
    Umiditate.update({ _id: id }, { $set: updateOps })
      .exec()
      .then(result => {
        console.log(result);
        res.status(200).json({
            message: 'Umiditatea a fost updatata',
            request: {
                type: 'GET',
                description: 'Pentru a vedea umiditatea Updatata',
                url: 'http://192.168.111.11:7001/umiditate/' + id,
                request: {
                    description: 'Pentru a vedea toate umiditatile',
                    url: 'http://192.168.111.11:7001/umiditate/'
                }
            }
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });

router.delete('/:umiditateId', (req, res, next) => {
    const id = req.params.umiditateId;
    Umiditate.deleteOne({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Umiditate stearsa',
                request: {
                    type: 'POST',
                    url: 'http://192.168.111.11:7001/umiditate/',
                    body: { value: 'Number', procent: 'String' }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
});

router.delete('/', (req, res, next) => {
    Umiditate.deleteMany()
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Intreaga colectie a fost stearsa',
                request: {
                    type: 'POST',
                    description: 'Pentru a crea o colectie noua',
                    url: 'http://192.168.111.11:7001/umiditate/',
                    body: { value: 'Number', procent: 'String' }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
});

router.patch("/:umiditateId", (req, res, next) => {
  const id = req.params.umiditateId;
  const updateOps = {};
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  Umiditate.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      console.log(result);
      res.status(200).json({
        message: 'Umiditatea a fost updatata',
        request: {
            type: 'GET',
            description: 'Pentru a vedea umiditatea Updatata',
            url: 'http://192.168.111.11:7001/umiditate/' + id,
            request: {
                description: 'Pentru a vedea toate umiditatile',
                url: 'http://192.168.111.11:7001/umiditate/'
            }
        }
    });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

module.exports = router;