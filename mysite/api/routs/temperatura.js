const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const checAuth = require('../midelware/check-auth')
const api_url = 'http://api.openweathermap.org/data/2.5/weather?q=Iasi&units=metric&appid=cd91fe4c7663cdf5e402897afe8d81ca'
const Temperatura = require('../models/data.js');
const apiKey = 'cd91fe4c7663cdf5e402897afe8d81ca';
const apiKey1 ='a9bdff1a-ae2d-4a49-ab85-98effaaa8987'

router.get('/', function (req, res) {
    res.render('index', {lat: null, lon: null, pol: null, weather:null, error: null});
  })
  
router.post('/', function (req, res) {
    let city = req.body.city;
    let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${apiKey}`
    console.log(req.body.city)
    request(url, function (err, response, body) {
      if(err){
        res.render('index', {weather: null, error: 'Error, please try again 1'});
      } else {
        let weather = JSON.parse(body)
        console.log("body:", weather)
        if(weather.main == undefined){
          res.render('index', {weather: null, error: 'Error, please try again 2'});
        } else {
          let weatherText = `It's ${weather.main.temp} degrees with ${weather.weather[0].main} in ${weather.name}!`;
          res.render('index', {weather: weatherText, error: null});
          console.log("Lat:", weather.coord.lat)
          console.log("Long:", weather.coord.lon)
          console.log("body_weather:", body)
          let lat = req.weather.coord.lat
          let lon = req.weather.coord.lon
          let url1 = `http://api.airvisual.com/v2/nearest_city?lat=${lat}&lon=${lon}&key=${apiKey1}`
          request(url1, function (response, body) {
            let pol = JSON.parse(body)
            console.log("body_pol:", pol)
            console.log("Calitatea aerului", pol.data.current.pollution.aqius)
          }
        )}
      }
    })
  //  .then(url1, function (err, response, body) {
  //    if(err){
  //      res.render('index2', {pol: null, error: 'Error, please try again 1'});
  //    } else {
  //      let pol = JSON.parse(body)
  //      console.log("body:", pol)
  //      if(pol.data == undefined){
  //        res.render('index2', {pol: null, error: 'Error, please try again 2'});
  //      } else {
  //        let polText = `Air quality is ${pol.data.current.pollution.aqius} at ${pol.data.current.pollution.ts} in ${pol.data.city}!`;
  //        res.render('index2', {pol: polText, error: null});
  //        console.log("body:", body)
  //      }
  //    }
  //  });
})
//router.post('/', function (req, res) {
//    let city = req.body.city;
//    let state = req.body.state;
//    let country = req.body.country;
//    let url1 = `http://api.airvisual.com/v2/city?city=${city}&state=${state}&country=${country}&key=${apiKey1}`
//    console.log(req.body.city)
//    request(url1, function (err, response, body) {
//      if(err){
//        res.render('index', {pol: null, error: 'Error, please try again 1'});
//      } else {
//        let pol = JSON.parse(body)
//        console.log("body:", pol)
//        if(pol.data == undefined){
//
//          res.render('index', {pol: null, error: 'Error, please try again 2'});
//        } else {
//          let dataText = `Air quality is ${pol.data.current.pollution.aqius} at ${pol.data.current.pollution.ts} in ${pol.data.city}!`;
//          res.render('index', {pol: dataText, error: null});
//          console.log("body:", body)
//        }
//      }
//    });
//  })
//
router.get('/', (req, res, next) => {
    Temperatura.find()
        .select('value type _id')
        .exec()
        .then(docs => {
            console.log(docs);
            const response = {
                count: docs.length,
                tempertaura: docs.map(doc => {
                    return {
                        value: doc.value,
                        type: doc.type,
                        _id: doc._id,
                        request: {
                            type: 'GET',
                            url: 'http://192.168.111.11:7001/temperatura/' + doc._id
                        }

                    }
                })
            }
            res.status(200).json(response);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
})

router.get('/:temperaturaId', (req, res, next) => {
    const id = req.params.temperaturaId;
    Temperatura.findById(id)
        .select('value type _id')
        .exec()
        .then(doc => {
            console.log('From DB', doc);
            if (doc) {
                res.status(200).json({
                    value: doc,
                    request: {
                        type: 'GET',
                        description: 'Afiseaza toate temperaturile',
                        url: 'http://192.168.111.11:7001/temperatura/'
                    }
                });
            } else {
                res.status(404).json({message: 'Datele nu exista'});
            }
            
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
});

router.post('/', (req, res, next) => {
   const tempertaura = new Temperatura({
       _id: new mongoose.Types.ObjectId(),
       value: req.body.value,
       type: req.body.type
   });
   tempertaura
   .save()
   .then(result => {
       console.log(result);
       res.status(200).json({
        message: 'POST temp',
        createdTemperatura: {
            value: result.value,
            type: result.type,
            _id: result._id,
            request: {
                type: 'GET the new item',
                url: 'http://192.168.111.11:7001/temperatura/' + result._id
            } 
        }
    });
   })
   .catch(err => console.log(err => {
       console.log(err);
       res.status(500).json({
           error: err
       })
   }));
});

router.put('/', (req, res, next) => {
    const tempertaura = new Temperatura({
        _id: new mongoose.Types.ObjectId(),
        value: req.body.value,
        type: req.body.type
    });
    tempertaura
    .save()
    .then(result => {
        console.log(result);
        res.status(200).json({
         message: 'PUT new item',
         createdTemperatura: {
            value: result.value,
            type: result.type,
            _id: result._id,
            request: {
                type: 'GET the new item',
                url: 'http://192.168.111.11:7001/temperatura/' + result._id
            } 
        }
     });
    })
    .catch(err => console.log(err => {
        console.log(err);
        res.status(500).json({
            error: err
        })
    }));
 });

router.put('/:temperaturaId', (req, res, next) => {
    const id = req.params.temperaturaId;
    const updateOps = {};
    for (const ops of req.body) {
      updateOps[ops.propName] = ops.value;
    }
    Temperatura.update({ _id: id }, { $set: updateOps })
      .select('value type _id')
      .exec()
      .then(result => {
        console.log(result);
        res.status(200).json({
            message: 'Temperatura a fost updatata',
            request: {
                type: 'GET',
                description: 'Pentru a vedea temperatura Updatata',
                url: 'http://192.168.111.11:7001/temperatura/' + id,
                request: {
                    description: 'Pentru a vedea toate temperaturile',
                    url: 'http://192.168.111.11:7001/temperatura/'
                }
            }
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });

router.delete('/:temperaturaId', (req, res, next) => {
    const id = req.params.temperaturaId;
    Temperatura.deleteOne({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Temperatura stearsa',
                request: {
                    type: 'POST',
                    url: 'http://192.168.111.11:7001/temperatura/',
                    body: { value: 'Number', type: 'String' }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
});

router.delete('/', (req, res, next) => {
    Temperatura.deleteMany()
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Intreaga colectie a fost stearsa',
                request: {
                    type: 'POST',
                    description: 'Pentru a crea o colectie noua',
                    url: 'http://192.168.111.11:7001/temperatura/',
                    body: { value: 'Number', type: 'String' }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
});

router.patch("/:temperaturaId", (req, res, next) => {
  const id = req.params.temperaturaId;
  const updateOps = {};
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  Temperatura.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      console.log(result);
      res.status(200).json({
        message: 'Temperatura a fost updatata',
        request: {
            type: 'GET',
            description: 'Pentru a vedea temperatura Updatata',
            url: 'http://192.168.111.11:7001/temperatura/' + id,
            request: {
                description: 'Pentru a vedea toate temperaturile',
                url: 'http://192.168.111.11:7001/temperatura/'
            }
        }
    });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

module.exports = router;