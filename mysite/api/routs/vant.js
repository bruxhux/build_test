const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');


const Vant = require('../models/vant.js');


router.get('/', (req, res, next) => {
    Vant.find()
        .exec()
        .then(docs => {
            console.log(docs);
            const response = {
                count: docs.length,
                vant: docs.map(doc => {
                    return {
                        value: doc.value,
                        unitate: doc.unitate,
                        _id: doc._id,
                        request: {
                            type: 'GET',
                            url: 'http://192.168.111.11:7001/vant/' + doc._id
                        }

                    }
                })
            }
            res.status(200).json(response);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
})

router.get('/:vantId', (req, res, next) => {
    const id = req.params.vantId;
    Vant.findById(id)
        .exec()
        .then(doc => {
            console.log('From DB', doc);
            if (doc) {
                res.status(200).json({
                    value: doc,
                    request: {
                        type: 'GET',
                        description: 'Afiseaza vantul',
                        url: 'http://192.168.111.11:7001/vant/'
                    }
                });
            } else {
                res.status(404).json({message: 'Datele nu exista'});
            }
            
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
});

router.post('/', (req, res, next) => {
   const vant = new Vant({
       _id: new mongoose.Types.ObjectId(),
       value: req.body.value,
       unitate: req.body.unitate
   });
   vant
   .save()
   .then(result => {
       console.log(result);
       res.status(200).json({
        message: 'POST temp',
        createdVant: {
            value: result.value,
            unitate: result.unitate,
            _id: result._id,
            request: {
                type: 'GET the new item',
                url: 'http://192.168.111.11:7001/vant/' + result._id
            } 
        }
    });
   })
   .catch(err => console.log(err => {
       console.log(err);
       res.status(500).json({
           error: err
       })
   }));
});

router.put('/:vantId', (req, res, next) => {
    const id = req.params.vantId;
    const updateOps = {};
    for (const ops of req.body) {
      updateOps[ops.propName] = ops.value;
    }
    Vant.update({ _id: id }, { $set: updateOps })
      .exec()
      .then(result => {
        console.log(result);
        res.status(200).json({
            message: 'Unitatea a fost updatata',
            request: {
                type: 'GET',
                description: 'Pentru a vedea unitatea Updatata',
                url: 'http://192.168.111.11:7001/vant/' + id,
                request: {
                    description: 'Pentru a vedea toate unitatile',
                    url: 'http://192.168.111.11:7001/vant/'
                }
            }
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  });

router.delete('/:vantId', (req, res, next) => {
    const id = req.params.vantId;
    Vant.deleteOne({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Unitate stearsa',
                request: {
                    type: 'POST',
                    url: 'http://192.168.111.11:7001/vant/',
                    body: { value: 'Number', unitate: 'String' }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
});

router.delete('/', (req, res, next) => {
    Vant.deleteMany()
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Intreaga colectie a fost stearsa',
                request: {
                    type: 'POST',
                    description: 'Pentru a crea o colectie noua',
                    url: 'http://192.168.111.11:7001/vant/',
                    body: { value: 'Number', unitate: 'String' }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
});

router.patch("/:vantId", (req, res, next) => {
  const id = req.params.vantId;
  const updateOps = {};
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  Vant.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      console.log(result);
      res.status(200).json({
        message: 'Unitatea a fost updatata',
        request: {
            type: 'GET',
            description: 'Pentru a vedea unitatea Updatata',
            url: 'http://192.168.111.11:7001/vant/' + id,
            request: {
                description: 'Pentru a vedea toate unitatile',
                url: 'http://192.168.111.11:7001/vant/'
            }
        }
    });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

module.exports = router;