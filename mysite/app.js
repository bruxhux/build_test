const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const request = require('request');
const apiKey = 'cd91fe4c7663cdf5e402897afe8d81ca';
const apiKey1 ='a9bdff1a-ae2d-4a49-ab85-98effaaa8987'

const mediuRoutes = require('./api/routs/mediu');
const umiditateRoutes = require('./api/routs/umiditate');
const vantRoutes = require('./api/routs/vant');
const temperaturaRoutes = require('./api/routs/temperatura');
const userRouts = require('./api/routs/user');

mongoose.connect('mongodb+srv://admin:admin@node-api-iupao.mongodb.net/test?retryWrites=true&w=majority');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/mediu', mediuRoutes);
app.use('/umiditate', umiditateRoutes);
app.use('/vant', vantRoutes);
app.use('/temperatura', temperaturaRoutes);
app.use('/user', userRouts);
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs')
 
app.get('/', function (req, res) {
  res.render('index', {pol: null, weather: null, error: null});
})

app.post('/', function (req, res) {
  let city = req.body.city;

  let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${apiKey}`

  console.log(req.body.city)
  request(url, function (err, response, body) {
    if(err){
      res.render('index', {weather: null, error: 'Error, please try again 1'});
    } else {
      let weather = JSON.parse(body)
      console.log("body:", weather)
      if(weather.main == undefined){
        res.render('index', {weather: null, error: 'Error, please try again 2'});
      } else {
        let weatherText = `It's ${weather.main.temp} degrees with ${weather.weather[0].main} in ${weather.name}!`;
        res.render('index', {weather: weatherText, error: null});
        console.log("body_weather:", body)
        console.log("Lat:", weather.coord.lat)
        console.log("Long:", weather.coord.lon)
        let lat = req.weather.coord.lat
        let lon = req.weather.coord.lon
        let url1 = `http://api.airvisual.com/v2/nearest_city?lat=${lat}&lon=${lon}&key=${apiKey1}`
        request(url1, function (body, response) {
          let pol = JSON.parse(body)
          console.log("body_pol:", pol)
          console.log("Calitatea aerului", pol.data.current.pollution.aqius)
        })
      }
    }
  });
 // request(url1, function (err, response, body) {
 //   if(err){
 //     res.render('index2', {pol: null, error: 'Error, please try again 1'});
 //   } else {
 //     let pol = JSON.parse(body)
 //     console.log("body:", pol)
 //     if(pol.data == undefined){
//
 //       res.render('index2', {pol: null, error: 'Error, please try again 2'});
 //     } else {
 //       let polText = `Air quality is ${pol.data.current.pollution.aqius} at ${pol.data.current.pollution.ts} in ${pol.data.city}!`;
 //       res.render('index2', {pol: polText, error: null});
 //       console.log("body:", body)
 //       
 //     }
   // }
  //});
})
//app.post('/', function (req, res) {
//  let city = req.body.city;
//  let state = req.body.state;
//  let country = req.body.country;
//  let url1 = `http://api.airvisual.com/v2/city?city=${city}&state=${state}&country=${country}&key=${apiKey1}`
//  console.log(req.body.city)
//  request(url1, function (err, response, body) {
//    if(err){
//      res.render('index', {pol: null, error: 'Error, please try again 1'});
//    } else {
//      let pol = JSON.parse(body)
//      console.log("body:", pol)
//      if(pol.data == undefined){
//
//        res.render('index', {pol: null, error: 'Error, please try again 2'});
//      } else {
//        let dataText = `Air quality is ${pol.data.current.pollution.aqius} at ${pol.data.current.pollution.ts} in ${pol.data.city}!`;
//        res.render('index', {pol: dataText, error: null});
//        console.log("body:", body)
//      }
//    }
//  });
//})


app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;